<?php

require('config.php');

$ip_address = $_SERVER['REMOTE_ADDR'] ?? null;
$user_agent = $_SERVER['HTTP_USER_AGENT'] ?? null;
$page_url = $_SERVER['HTTP_REFERER'] ?? null;

// Not sure if we are interested in empty values, assumed that we are not
if ($ip_address && $user_agent && $page_url) {
    try {
        $mysqli = new mysqli(MYSQL_HOSTNAME, MYSQL_USERNAME, MYSQL_PASSWORD, MYSQL_DATABASE);

        $sql = "INSERT INTO banner_views (ip_address, page_url, user_agent)
                VALUES (INET_ATON(?), ?, ?)
                ON DUPLICATE KEY UPDATE views_count=views_count+1";
                
        $stmt = $mysqli->prepare($sql);
        $stmt->bind_param('sss', $ip_address, $page_url, $user_agent);
        $stmt->execute();

    } catch (Exception $e) {
        $error = date('Y-m-d H:i:s')." {$e->getMessage()}\n";
        $ferrors = fopen('errors.log', 'at');
        fwrite($ferrors, $error);
        fclose($ferrors);
    }
}

header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');
header('Pragma: no-cache');

header('Content-Type: image/png');
header('Content-Length: '.filesize(BANNER_FILENAME));

readfile(BANNER_FILENAME);

?>