# INFUSEmedia PHP Test Task



## Clone the project

```
cd <your_projects_dir>
git clone https://gitlab.com/alexander.babayev/infusemedia-php-test-task.git banner
cd banner
```

## Configure the app

Open and edit the `config.php` file

```
define('MYSQL_HOSTNAME', 'localhost');
define('MYSQL_USERNAME', 'root');
define('MYSQL_PASSWORD', '');
define('MYSQL_DATABASE', 'banner');

define('BANNER_FILENAME', './php-logo-small.png');
```

## Create a MySQL database

Establish connection to your MySQL server:
```
mysql -u root -p
```

And execute next commands:
```
create database banner;
use banner
source <your_projects_dir>/banner/database.sql
```
